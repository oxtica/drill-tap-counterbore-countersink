# Drill, Tap, Counterbore and Countersink

Technical resources for manufacturing professionals

* Drilling, Tapping, Counterboring, Countersinking for Metalworking
* ANSI Metric
* Metric Socket Head Cap Screws

|No |Screw Diameter|Tap Size|Drill Size|Counterbore Diameter|Counterbore Depth|Countersink Diameter|Clearance Diameter (Normal Fit)|Clearance Diameter (Close Fit)|Bolt Torque - Nm (Dry)|Bolt Torque - Nm (Lubed)|
|----|----|----------|-------|--------|-----|--------|-------|--------|----|----|
|1 |M2  |M2 X 0.4  |1.6 mm |4.4 mm  |2 mm |2.6 mm  |2.4 mm |2.2 mm  |    |    |
|2 |M3  |M3 X 0.5  |2.5 mm |6.5 mm  |3 mm |3.6 mm  |3.7 mm |3.4 mm  |    |    |
|3 |M4  |M4 X 0.7  |3.3 mm |8.25 mm |4 mm |4.7 mm  |4.8 mm |4.4 mm  |    |    |
|4 |M5  |M5 X 0.8  |4.2 mm |9.75 mm |5 mm |5.7 mm  |5.8 mm |5.4 mm  |10.3|7.7 |
|5 |M6  |M6 X 1    |5   mm |11.2 mm |6 mm |6.8 mm  |6.8 mm |6.4 mm  |17.6|13.1|
|6 |M8  |M8 X 1.25 |6.8 mm |14.5 mm |8 mm |9.2 mm  |8.8 mm |8.4 mm  |42.6|32.1|
|7 |M10 |M10 X 1.5 |8.5 mm |17.5 mm |10 mm|11.2 mm |10.8 mm|10.5 mm |84  |64  |
|8 |M12 |M12 X 1.75|10.2 mm|19.5 mm |12 mm|14.2 mm |13 mm  |12.5 mm |146 |110 |
|9 |M14 |M14 X 2   |12 mm  |22.5 mm |14 mm|16.2 mm |15 mm  |14.5 mm |235 |176 |
|10|M16 |M16 X 2   |14 mm  |25.5 mm |16 mm|18.2 mm |17 mm  |16.5 mm |365 |274 |
|11|M20 |M20 X 2.5 |17.5 mm|31.5 mm |20 mm|22.4 mm |21 mm  |20.5 mm |712 |534 |
|12|M24 |M24 X 3   |37.5 mm|37.5 mm |24 mm|26.4 mm |25 mm  |24.5 mm |1231|923 |
